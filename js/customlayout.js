// Custom Jquery code for the website
  // Set banner height

jQuery.noConflict()(document).ready(function( $ ) {
  const resizeBannerImages = () => {
    if ($('.slogan').length === 0) {
      return;
    }
    const slogan_height =  $('.slogan .last-line').height() + $('.slogan .last-line').position().top + 20;
    $('.slogan').height(slogan_height);
	  $('.banner img').each((index, value) => {
		  $(value).height(slogan_height);
    });
  }

  resizeBannerImages();
  $(window).resize( () => {
    resizeBannerImages();
  });


  $(".owl-carousel").owlCarousel({
    loop: false,
    margin: 10,
    nav: false,
    center: false,
    dots: true,
    responsive: {
      0: {
        items: 1
      },
      600: {
        items: 2
      },
      900: {
        items: 3
      },
      1200: {
        items: 4
      }
    }
  });

  const openCloseMenu = () => {
    $(".hamburger-menu").toggleClass("open");
    $(".overlay").toggleClass("active");
    $("#menu-container").toggleClass("active");
    fixTopMenuWidth($("#menu-container .navbar-dark"));
  }

  const fixTopMenuWidth = myObj => {
    const parentWidth = myObj.parent().width();
    myObj.css("width", parentWidth);
  };

  $(document.body).on('click touchend', '.accordion-toggle .menuitem, .go-back', (event) => {
    const parentTarget = $(event.currentTarget).closest('.accordion-toggle');
    const submenu = parentTarget.find(".accordion-content");
    submenu.toggleClass("active");
    parentTarget.find(".topmenu").toggleClass("fixed");
    $(".accordion-toggle")
      .not(parentTarget)
      .toggleClass("d-none");
    $(".go-back").toggleClass("d-none");
    fixTopMenuWidth(parentTarget.find(".topmenu"));
    return false;
  });

  $(".accordion-toggle a ").on('click', () => {
    openCloseMenu();
    return false;
  });

  $(window).on("orientationchange", function(event) {
    $("#menu-container").removeClass("active");
    $(".accordion-toggle .topmenu")
      .removeClass("d-none")
      .css("width", "");
    $("#menu-container .navbar-dark").css("width", "");
    $(".hamburger-menu").removeClass("open");
    $(".overlay").removeClass("active");
  });

  $(document.body).on('click', '.dismiss' , function(event) {
    openCloseMenu();
    return false;
  });

  $(document.body).on('click touchend', '.open-menu', function(event) {
    openCloseMenu();
    return false;
  });

  $('.search.form-inline').on('submit', (event) => {
    const overlayElement = $('.overlay');
    if (overlayElement.length == 1 && overlayElement.hasClass('active')) {
      openCloseMenu();
      return false;
    }
  });

});
