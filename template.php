<?php

/**
 * @file
 * Bootstrap sub-theme.
 *
 * Place your custom PHP code in this file.
 */

/**
 * Implements hook_html_head_alter().
 * Call this Drupal 7 API function to remove the Drupal 7 generator meta tag
 */
function copdukenursing_html_head_alter(&$head_elements)
{
  // Remove Drupal generator meta tag.
  // Use this if you want to remove the Drupal 7 Generator meta tag.
  if (isset($head_elements['system_meta_generator'])) {
    unset($head_elements['system_meta_generator']);
  }
}


/**
 * Create a page for each content type
 * add nav tabs in admin mode
 */
function copdukenursing_preprocess_page(&$vars)
{
  if (isset($vars['node']->type)) {
    // If the content type's machine name is "my_machine_name" the file
    // name will be "page--my-machine-name.tpl.php".
    $vars['theme_hook_suggestions'][] = 'page__' . $vars['node']->type;
  }
  $vars['primary_local_tasks'] = $vars['tabs'];
  unset($vars['primary_local_tasks']['#secondary']);
  $vars['secondary_local_tasks'] = array(
    '#theme' => 'menu_local_tasks',
    '#secondary' => $vars['tabs']['#secondary'],
  );
  unset($vars['page']['hidden']);
}

function copdukenursing_preprocess_views_view(&$vars)
{
  if (isset($vars['name'])) {
    $vars['theme_hook_suggestions'][] = 'views_view__' . $vars['name'];
  }
}

/* remove width and height from img calls from field */
function copdukenursing_preprocess_image(&$variables) {
  foreach(array('width', 'height') as $key) {
    unset($variables[$key]);
  }
}

/* remove add new comment from the node
 */
function copdukenursing_preprocess_node(&$variables)
{
  unset($variables['content']['links']);
}
//function copdukenursing_css_alter(&$css) {
//  $css = array("https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css");
//}
//

// This prints out the LI element of the Cop Main Page
// Some hacking was done to accomodate both the Desktop view and the Mobile view
// this is achieved by looking at the depth of the link. If it is 1, the its top menu
// if 2 it is the 2nd level. Error checking is done to ensure anything greater than 2 is not printed.
function copdukenursing_menu_link__menu_field_article_category($variables)
{
  $output = '';
  $element = $variables['element'];
  $href = $element['#href'];
  if ($element['#original_link']['depth'] == 1) {
    $output = '<li class="nav-item big-screen ' . implode(' ', $element['#attributes']['class']) .
      '"><a class="nav-link" href="' . url(drupal_get_path_alias($href)) .
      '"><span class="spacer">' .
      $element['#title'] . '</span></a></li>';
    $output .= '<li class=" nav-item accordion-toggle '. implode(' ', $element['#attributes']['class']) .
      '"><div class="nav-link topmenu "><div class="go-back d-none"><i class="fas fa-angle-left">'.
      '</i>&nbsp;Back</div><div class="menuitem d-flex"><span class="mr-auto pl-2 spacer">'.
      $element['#title']. '</span><i class="rotate-down fas fa-angle-right tertiary-grey1 mr-4"></i>'.
      '</div></div>';
    $output .= drupal_render($element['#below']);
    $output .= '</li>';
  } else {
    array_push($element['#attributes']['class'], 'nav-item');
    $output .= '<li '. drupal_attributes($element['#attributes']) .
      '"><a class="nav-link ajax-link" href="' . url(drupal_get_path_alias($href)) .
      '">' . $element['#title'] . '</a></li>';
  }
  return $output;
}

// This function out the UL html tag structure for the styling of the CoP Main menu
// prints out the following UL depending on whether it is a
// top level menu or a 2nd level menu
// *****
// Top level menu print following ul
// <ul class="navbar-nav">
// [ li elements]
// </ul>
// ****
// 2nd Level menu print the following ul
//  <ul class="menu-submenu accordion-content flex-column nav">
// [ ii elements]
// <ul>
//
function copdukenursing_menu_tree__menu_field_article_category($variables)
{
  $output = '';
  $depth = current($variables['#tree'])['#original_link']['depth'];
  if ($depth == 1) {
    $output .= '<ul class="navbar-nav">';
    $output  .= $variables['tree'];
  } else {
    $output .= '<ul class="menu-submenu accordion-content flex-column nav">';
    $output .= $variables['tree'];
  }
  $output .= '</ul>';
  return $output;
}


function copdukenursing_menu_link__menu_about_us($variables){
  $element = $variables['element'];
  $output = '<li class="nav-item">';
  $output .= '<a href="' . url(drupal_get_path_alias($element['#href'])) . '" class="nav-link">';
  $output .= $element['#title'];
  $output .= '</li>';
  return $output;
}

function copdukenursing_menu_tree__menu_about_us($variables)
{
  $output = '<div class="font-weight-bold">' . $variables['#tree']['#block']->subject . '</div>';
  $output .= '<ul class="nav flex-column">';
  $output .= $variables['tree'];
  $output .= '</ul>';
  return $output;
}

function copdukenursing_menu_tree__menu_resources($variables)
{
  $output = '<div class="font-weight-bold">' . $variables['#tree']['#block']->subject . '</div>';
  $output .= '<ul class="nav flex-column">';
  $output .= $variables['tree'];
  $output .= '</ul>';
  return $output;
}



function copdukenursing_menu_link__menu_resources($variables){
  $element = $variables['element'];
  $output = '<li class="nav-item">';
  $output .= '<a href="' . url(drupal_get_path_alias($element['#href']))  . '" class="nav-link">';
  $output .= $element['#title'];
  $output .= '</li>';
  return $output;
}

?>
