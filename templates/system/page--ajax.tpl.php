<?php if ($tabs = render($tabs)): ?><div class="tabs"><?php print $tabs; ?></div
><?php endif; ?>
<?php if ($action_links = render($action_links)): ?><ul class="action-links">
<?php print $action_links; ?></ul><?php endif; ?>
<?php print render($page['content']); ?>
	