<?php $display = array('label' => 'hidden'); ?>

<div class="page-body homepage">
	<div class="banner">
		<div class="row">
			<div class="d-none d-sm-flex col-sm-4 masked-right pl-0">
				<?php
				$field_banner_image = field_view_field('node', $node, 'field_banner_image_left', $display);
				print render($field_banner_image);
				?>

			</div>
			<div class="col-12 col-sm-4 px-0 text-center text-white">
				<div class="slogan">
					<div class="display-1 text-shadow">INSPIRE</div>
					<div class="h1">
						the <span class="font-italic">next generation </span>
					</div>
					<div class="h1 last-line">of nurse practitioners</div>
				</div>
			</div>
			<div class="d-none d-sm-flex col-sm-4 masked-left pr-0">
				<?php
				$field_banner_image_right = field_view_field('node', $node, 'field_banner_image_right', $display);
				print render($field_banner_image_right);
				?>

			</div>
		</div>
	</div>
	<div class="value-prop container mb-4 pb-4">
		<div class="value-prop1">

			<div class="prop1 row py-3 ">
				<div class="col-12 text-center col-sm-4 mt-2">
					<div class="h1 icon">
						<i class="fas fa-user-friends duke-orange"></i>
					</div>
					<div class="h5 text-uppercase duke-orange font-weight-bold">
						MENTOR
					</div>
					<div class="value-prop-text">
						<?php
						$field_value_prop_mentor = field_view_field('node', $node, 'field_value_prop_mentor', array('label' => 'hidden'));
						print render($field_value_prop_mentor);
						?>
					</div>
					<hr class="d-sm-none" />
				</div>
				<div class="col-12 col-sm-4 text-center mt-2 ">
					<div class="h1 icon">
						<i class="far fa-comments duke-yellow"></i>
					</div>
					<div class="h5 text-uppercase duke-yellow font-weight-bold">
						COLLABORATE
					</div>
					<div class="value-prop-text">
						<?php
						$field_value_prop_collaborate = field_view_field('node', $node, 'field_value_prop_collaborate', array('label' => 'hidden'));
						print render($field_value_prop_collaborate);
						?>
					</div>
					<hr class="d-sm-none" />
				</div>
				<div class="col-12 col-sm-4 text-center mt-2">
					<div class="h1 icon">
						<i class="fas fa-building duke-blue"></i><i class="far fa-building duke-blue small"></i>
					</div>
					<div class="h5 text-uppercase duke-blue font-weight-bold">
						GIVE BACK
					</div>
					<div class="value-prop-text">
						<?php
						$field_value_prop_give_back = field_view_field('node', $node, 'field_value_prop_give_back', array('label' => 'hidden'));
						print render($field_value_prop_give_back);
						?>

					</div>
					<hr class="d-sm-none" />
				</div>
			</div>
		</div>


		<div class="value-prop2 my-4">
			<div class="row my-4">
				<div class="col-12 col-md-6">
					<div class="d-flex justify-content-center my-3">
						<div class="py-3 mt-lg-3">
							<a class="blue-button text-center" href="<?php print theme_get_setting('become_a_preceptor_url');  ?>">BECOME A PRECEPTOR</a>
						</div>
					</div>
					<div class="div text-center font-italic">
						<?php
						$field_value_prop_details = field_view_field('node', $node, 'field_value_prop_details', array('label' => 'hidden'));
						print render($field_value_prop_details);
						?>
					</div>
				</div>

				<div class="col-12 col-md-6 clinical-module">
					<hr class="d-sm-none" />

					<div class="duke-bg-tertiary-gray3  d-flex justify-content-center m-2 p-3 rounded h-100">
						<div class="row">
							<div class="col-12  mb-4">
								<div class="d-flex justify-content-center  mt-4">
									<a class="purple-button d-flex align-items-center" href="<?php
																																						print theme_get_setting('clinical_module_url');
																																						?>
									">
										<i class="fa-play-circle fa"></i>
										<span class="px-2 text-center">ACCESS <?php print strtoupper(theme_get_setting('clinical_module_title')); ?></span>
									</a>

								</div>

							</div>

							<div class="col-12 d-flex justify-content-center mb-4 text-center">
								<?php
								$field_clinical_mod_details = field_view_field('node', $node, 'field_clinical_mod_intro_details', array('label' => 'hidden'));
								print render($field_clinical_mod_details);

								?>
							</div>
						</div>
					</div>
				</div>
			</div>
			<hr class="d-sm-none" />
			<div class="value-prop3 mt-3 pt-3">
				<div class="row">
					<div class="col-12">
						<h1 class="text-uppercase text-center duke-blue font-weight-bold h4 mb-3">
							PRECEPTOR BENEFITS
						</h1>
						<?php
						$field_preceptor_benefits = field_view_field('node', $node, 'field_preceptor_benefits');
						print render($field_preceptor_benefits);
						?>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="faculty-statement-wrapper">
		<div class="faculty-statement">
			<div class="overlay-lighten">
				<div></div>
			</div>
			<div class="container">
				<div class="row">
					<div class="col-3 col-lg-2 offset-1 offset-lg-2">
						<div class="small-rounded-image">
							<?php
							$field_faculty_image = field_view_field('node', $node, 'field_faculty_image', $display);
							print render($field_faculty_image); ?>
						</div>
						<div class="image-name font-weight-bold mt-2 text-center ">
							<?php
							$field_faculty_name = field_view_field('node', $node, 'field_faculty_name', $display);
							print render($field_faculty_name); ?>
						</div>
					</div>
					<div class="col-8 col-lg-6">
						<div class="h4">From The Faculty...</div>
						<div class="quote-from-faculty">
							<i class="fas fa-quote-left pr-2"></i>
							<span class="font-italic">
								<?php
								$field_from_the_faculty = field_view_field('node', $node, 'field_from_the_faculty', $display);
								print render($field_from_the_faculty);
								?>
								<i class="fas fa-quote-right pl-2"></i>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>