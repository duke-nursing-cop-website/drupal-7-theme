<?php
$display = array('label' => 'hidden');

$t_node = $row->_field_data['nid']['entity'];

$youtube_url = field_view_field('node', $t_node, 'field_youtube_video_url')['#items'][0]['value'];

preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $youtube_url, $match);
$youtube_id = $match[1];


?>
<div class="card-parent col-12 col-sm-6 col-md-4 col-lg-3">
	<div class="card">
		<a class="ajax-link" href="<?php print url(drupal_get_path_alias('node/' . $t_node->nid)); ?>">
			<div class="card-header">
				<img src="<?php
									$y_img = 'https://img.youtube.com/vi/' . $youtube_id . '/0.jpg';
									print $y_img;
									?>" alt="" />
			</div>
			<div class="card-body">
				<h5 class="card-title">
					<?php print $t_node->title ?>
				</h5>
				<div class="card-text">
					<?php

					$field_subtitle = field_view_field('node', $t_node, 'field_subtitle', $display);
					print drupal_render($field_subtitle);
					?>
				</div>
			</div>
		</a>
	</div>
</div>