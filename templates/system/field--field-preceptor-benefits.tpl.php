<?php foreach ($items as $delta => $item) { ?>
  <div class="row mb-4">
    <div class="col-1 d-flex justify-content-end align-items-center px-0">
      <i class="duke-green fas fa-check-circle"></i>
    </div>
    <div class="col-11">
      <?php print render($item); ?>
    </div>
  </div>
<?php } ?>