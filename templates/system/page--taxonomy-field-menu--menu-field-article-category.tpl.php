

<?php

$tree_output = [];
$breadcrumb = menu_get_active_trail();
$cop_menu = $breadcrumb[1]['menu_name'];
$mlid = $breadcrumb[1]['mlid'];
$parent_tree = menu_tree_page_data($cop_menu);
foreach ($parent_tree as $key => $element) {
	if ($mlid == $element['link']['mlid']) {
    $tree_output = $element['below'];
    reset($tree_output)['link']['in_active_trail'] = 1;
		break;
	}
}

$active_node_path = reset($tree_output)['link']['link_path'];

$nodelist = $page['content']['system_main']['nodes']['nodes'];
$new_nodelist = [];
foreach ( $nodelist as $t_node) {
	$t_node_path = $t_node['links']['node']['#links']['node-readmore']['href'];
	if (strcmp($active_node_path, $t_node_path) == 0) {
		array_push($new_nodelist, $t_node);
		break;
	}
}

$page['content']['system_main']['nodes']['nodes'] = $new_nodelist;

?>

<?php  $edit_mode = isset($variables['page']['content']['system_main']['#node_edit_form']); ?>

<?php if ($logged_in) : ?>
<div id="navigation">

  <?php if ($primary_local_tasks): ?>
    <?php print render($primary_local_tasks); ?>
	<?php endif; ?>

  <?php if ($secondary_local_tasks): ?>
    <div class="tabs-secondary clearfix"><ul class="tabs secondary"><?php print render($secondary_local_tasks); ?></ul></div>
  <?php endif; ?>

</div>
<?php endif; ?>


<?php if (! $edit_mode) : ?>
<?php print render($page['header']); ?>
<?php endif; ?>


<div id="content">
<?php  print render($page['content']) ?>
</div>

<?php if (! $edit_mode): ?>
<?php print render($page['footer']); ?>
<?php endif; ?>
