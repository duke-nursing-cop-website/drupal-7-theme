<?php
$display = array('label' => 'hidden');
$current_tree = [];
$breadcrumb = menu_get_active_trail();
$article_category = $breadcrumb[1]['link_title'];
$page_color = $breadcrumb[1]['options']['item_attributes']['class'];
$cop_menu = $breadcrumb[1]['menu_name'];
$mlid = $breadcrumb[1]['mlid'];
$parent_tree = menu_tree_page_data($cop_menu);
foreach ($parent_tree as $key => $element) {
	if ($mlid == $element['link']['mlid']) {
		$current_tree = $element;
		break;
	}
}
?>



<div class="page-body article <?php print $page_color ?>-page">
	<div class="banner d-xl-none">
		<?php
		$field_banner_image = field_view_field('node', $node, 'field_banner_image');
		print render($field_banner_image);
		?>

	</div>
	<hr class="p-0 m-0 set-bg-color" />

	<div class="breadcrumb d-flex align-items-center duke-bg-blue text-white mb-0">
		<div class="open-menu d-inline-block"><i class="fas fa-home px-1"></i></div>
		<i class="fas fa-angle-right mx-1 "></i>
		<span class="open-menu d-inline-block"><?php print $article_category ?></span>
	</div>
	<div class="row">
		<div class="d-none d-lg-block col-lg-4 col-xl-3 duke-bg-tertiary-gray2 sidebar">
			<div class="container">
				<?php
				$tree_output = menu_tree_output($current_tree['below']);
				print drupal_render($tree_output);
				?>
			</div>
		</div>
		<div class="col-12 col-lg-8 col-xl-9 py-3 main">
			<div class="container mx-2">
				<div class="row">
					<div class="col-12 text-section-one text-section">
						<h2 class="pb-2"><?php print $title ?></h2>
						<div>
							<div class="rounded float-md-right pl-2 pb-2 m-1">
								<?php
								$field_article_image = field_view_field('node', $node, 'field_article_image');
								print render($field_article_image);
								?>


							</div>
							<?php

							$field_first_section = field_view_field('node', $node, 'field_first_section', $display);
							print render($field_first_section);
							?>
						</div>
					</div>
					<div class="col-12 emphasis">
						<div class="col-11 duke-med-blue spacer">
							<?php
							$field_emphasis = field_view_field('node', $node, 'field_emphasis', $display);
							print render($field_emphasis);
							?>

						</div>
					</div>
					<div class="col-12 text-section text-section-two">
						<h2>
							<?php
							$field_second_section_title = field_view_field('node', $node, 'field_second_section_title', $display);
							print render($field_second_section_title);

							?>

						</h2>
						<?php
						$field_second_section_text = field_view_field('node', $node, 'field_second_section', $display);
						print render($field_second_section_text);

						?>

					</div>
				</div>
			</div>
		</div>
	</div>
</div>