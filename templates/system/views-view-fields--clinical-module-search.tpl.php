<?php
$display = array('label' => 'hidden');

$t_node = $row->_entity_properties['entity object'];
$youtube_url = field_view_field('node', $t_node, 'field_youtube_video_url')['#items'][0]['value'];

preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $youtube_url, $match);
$youtube_id = $match[1];
$field_category_tid = $t_node->field_category['und'][0]['tid'];
$field_term = taxonomy_term_load($field_category_tid);
$field_name = $field_term->name;
?>

<div class="card mb-3">

	<div class="row no-gutters">
		<div class="col-4 col-sm-2 pt-3 pr-3">
			<a class="ajax-link" href="<?php print url(drupal_get_path_alias('node/' . $t_node->nid)); ?>">

				<img src="<?php
									$y_img = 'https://img.youtube.com/vi/' . $youtube_id . '/0.jpg';
									print $y_img;
									?>" alt="" />
			</a>
		</div>
		<div class="col-8 col-sm-8 pt-2">
			<div class="card-body">
				<a class="ajax-link" href="<?php print url(drupal_get_path_alias('node/' . $t_node->nid)); ?>">

					<h5 class="card-title">
						<?php print $t_node->title ?>
					</h5>
					<p class="card-text">
						<?php

						$field_subtitle = field_view_field('node', $t_node, 'field_subtitle', $display);
						print drupal_render($field_subtitle);
						?>
					</p>
				</a>
				<p class="card-text mt-3">
					<small class="text-muted">
						<a href="<?php print url(drupal_get_path_alias('taxonomy/term/' . $field_category_tid)); ?>">
							<?php print $field_name ?>
						</a>
					</small>
				</p>
			</div>
			</a>
		</div>
	</div>
</div>