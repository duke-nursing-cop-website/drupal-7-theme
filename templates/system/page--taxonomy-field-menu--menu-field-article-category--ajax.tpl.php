

<?php
$tree_output = [];
$breadcrumb = menu_get_active_trail();
$cop_menu = $breadcrumb[1]['menu_name'];
$mlid = $breadcrumb[1]['mlid'];
$parent_tree = menu_tree_page_data($cop_menu);
foreach ($parent_tree as $key => $element) {
	if ($mlid == $element['link']['mlid']) {
    $tree_output = $element['below'];
    reset($tree_output)['link']['in_active_trail'] = 1;
		break;
	}
}

$active_node_path = reset($tree_output)['link']['link_path'];

$nodelist = $page['content']['system_main']['nodes']['nodes'];
$new_nodelist = [];
foreach ( $nodelist as $t_node) {
	$t_node_path = $t_node['links']['node']['#links']['node-readmore']['href'];
	if (strcmp($active_node_path, $t_node_path) == 0) {
		array_push($new_nodelist, $t_node);
		break;
	}
}

$page['content']['system_main']['nodes']['nodes'] = $new_nodelist;

?>

<?php if ($tabs = render($tabs)): ?><div class="tabs"><?php print $tabs; ?></div
><?php endif; ?>
<?php if ($action_links = render($action_links)): ?><ul class="action-links">
<?php print $action_links; ?></ul><?php endif; ?>
<?php print render($page['content']); ?>

