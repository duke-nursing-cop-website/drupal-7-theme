
<div class="page-body clinical-search">
  <hr />
  <div class="breadcrumb align-items-center duke-bg-blue text-white mb-0 mr-auto">
    <div class="mr-auto">
		  <div class="open-menu d-inline-block"><i class="fas fa-home px-1"></i></div>
      <i class="fas fa-angle-right mx-1 "></i>
      <span class="open-menu">Clinical Modules</span>
      <i class="fas fa-angle-right mx-1 "></i>
      <span class="">Search</span>
    </div>
    <form class="search form-inline flex-nowrap">
      <input class="form-control" type="text" placeholder="Search Modules" />
      <button class="search-button mx-0" type="submit">
        <i class="fas fa-search"></i>
      </button>
    </form>
  </div>
  <div class="main clinical-search">
    <div class="row">
      <div class="col-12 mt-3">
        <h3 class="text-center">Search Results</h3>
        <hr />
      </div>
      <div class="col-12 offset-sm-2 col-sm-10">
        <div class="my-3 module-list row">
        <?php
          if ($rows) {
            print $rows;
          } else {
        ?>
         
        <div class="text-center h5">
          No clinical modules found.
        </div>
        <?php   
          }
          ?>
          <div class="col-12">
          <?php
          if ($pager) {
            print $pager;
          }
          ?>
        </div>
        </div>
      </div>
    </div>
  </div>