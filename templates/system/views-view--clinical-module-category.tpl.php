<?php
$vid = '';
$all_vocabs = taxonomy_get_vocabularies();
foreach ($all_vocabs as $vocab) {
  if ($vocab->machine_name == 'clinical_module_categories') {
    $vid = $vocab->vid;
    break;
  }
}
$clinical_module_terms = taxonomy_get_tree($vid);

$tid = $view->args[0];
?>
<div class="page-body clinical-home">
  <!-- <div class="d-lg-none banner  align-items-center text-center">
    <div class="row w-100 position-absolute">

      <div class="banner-text col-8 col-sm-5 offset-sm-3 ml-sm-5 mt-2 mt-sm-4">
        <div>CONTINUE DEVELOPING YOUR</div>
        <div>SKILLS FROM ANYWHERE WITH</div>
        <div>CLINICAL MODULES.</div>
      </div>
    </div>
    <img src="https://lh3.googleusercontent.com/QryIX_LDVZx3q7G1Gyo61qDhGRI-AAi8PjxJOKHEGIVa6vUGzpzKpUzhDnmV0MtD9g3TLrVIIZc" class="masked-left w-100" alt="" />
  </div> -->
  <hr />
  <div class="breadcrumb align-items-center duke-bg-blue text-white mb-0 mr-auto">
    <div class="mr-auto">
      <div class="open-menu d-inline-block"><i class="fas fa-home px-1"></i></div>
      <i class="fas fa-angle-right mx-1 "></i>
      <span class="open-menu"><?php print theme_get_setting('clinical_module_title');  ?></span>
    </div>
    <form class="search form-inline flex-nowrap">
      <input name="search-field" class="form-control" type="text" placeholder="Search Modules" />
      <button class="search-button mx-0" type="submit">
        <i class="fas fa-search"></i>
      </button>
    </form>
  </div>
  <div class="clinical-home">
    <div class="row">
      <div class="col-lg-3  d-none d-lg-block duke-bg-tertiary-gray2 sidebar">
        <div class="container pl-2 pt-3 ">
          <h3 class="mb-3 pb-1">Categories</h3>
          <ul>
            <?php foreach ($clinical_module_terms as $module_term) { ?>
              <li>
                <a class="ajax-link <?php
                                      if ($tid == $module_term->tid) {
                                        print 'active-trail';
                                      } ?>" href="<?php
                                                  print url(drupal_get_path_alias('taxonomy/term/' . $module_term->tid));
                                                  ?>">
                  <?php print $module_term->name; ?>
                </a>
              </li>
            <?php } ?>
          </ul>
        </div>
      </div>
      <div class="col-12 col-lg-9 main">
        <form class="form-inline categories-select row">
          <div class="col-12 d-flex justify-content-end align-items-center">
            <label class="my-1 pr-1" for="inlineFormCustomSelectPref">Category:</label>
            <select class="custom-select my-1 mr-sm-2" id="inlineFormCustomSelectPref">
              <?php foreach ($clinical_module_terms as $module_term) { ?>
                <option <?php if ($tid == $module_term->tid) {
                            print "selected";
                          } ?> value="<?php print url(drupal_get_path_alias('taxonomy/term/' . $module_term->tid)) ?>">
                  <?php print $module_term->name ?>
                </option>
              <?php } ?>

            </select>
          </div>

        </form>
        <div class="my-3 module-list row">

          <?php
          if ($rows) {
            print $rows;
          }
          ?>
        </div>
        <div class="col-12">
          <?php
          if ($pager) {
            print $pager;
          }
          ?>
        </div>

      </div>
    </div>
  </div>
</div>