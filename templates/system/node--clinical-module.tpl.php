<?php
$display = array('label' => 'hidden');
$trail = menu_get_active_trail();

$lastInTrail = end($trail);
$category = field_view_field('node', $node, 'field_category', $display);
$t_object = taxonomy_get_term_by_name($category[0]['#title']);
$tid = key($t_object);
$nids = taxonomy_select_nodes($tid, FALSE);
$t_nodes = node_load_multiple($nids);
//foreach ($nodes as $t_node) {
//	print $t_node->title;
//	print drupal_get_path_alias('node/' . $t_node->nid);
//}

$menu_name = 'main_menu';
$parent_tree = menu_tree_page_data($menu_name);
$tree = menu_tree_all_data('menu-cop-menu');
$page_color = 'duke-med-blue';

// hard code strip of color for clinical modules to duke-purple
$page_color = 'duke-purple';

// generate youtube_id
$youtube_url = field_view_field('node', $node, 'field_youtube_video_url')['#items'][0]['value'];
preg_match('%(?:youtube(?:-nocookie)?\.com/(?:[^/]+/.+/|(?:v|e(?:mbed)?)/|.*[?&]v=)|youtu\.be/)([^"&?/ ]{11})%i', $youtube_url, $match);
$youtube_id = $match[1];

?>


<div class="page-body clinical-module <?php print $page_color ?>-page">
	<hr class="p-0 m-0 set-bg-color" />

	<div class="breadcrumb d-flex align-items-center duke-bg-blue text-white mb-0">
		<div class="open-menu d-inline-block"><i class="fas fa-home px-1"></i></div>
		<i class="fas fa-angle-right mx-1 "></i>
		<!-- Get clinical module home page href from grabbing whole menu -->
		<span><a href="<?php print url(drupal_get_path_alias($category[0]['#href'])) ?>"><?php print $category[0]['#title'] ?></a></span>
	</div>

	<div class="bg-white container-fluid pl-0">
		<div class="row">
			<div class="d-none d-xl-block col-xl-3 sidebar pt-3 duke-bg-tertiary-gray2">
				<ul class="pl-0">
					<?php foreach ($t_nodes as $t_node) { ?>
						<li>
							<?php
								$active_trail_class = "";
								if ($node->nid == $t_node->nid) :
									$active_trail_class = "active-trail";
								endif;
								?>

							<a class="ajax-link <?php print $active_trail_class ?>" href="<?php print url(drupal_get_path_alias('node/' . $t_node->nid)); ?>">
								<?php print $t_node->title ?>
							</a>
						</li>
					<?php } ?>
				</ul>
			</div>

			<div class="col-12 col-xl-9 main">
				<div class="row pt-3 pb-1">
					<div class="col-12 col-lg-8 rounded p-2 module-header set-bg-color text-white font-weight-bold">
						<?php print $node->title ?>
					</div>
				</div>
				<div class="row py-3 align-items-center">
					<div class="col-12 col-lg-8">
						<div class="embed-responsive embed-responsive-16by9">
							<iframe class="embed-responsive-item" src="https://www.youtube.com/embed/<?php print $youtube_id ?>" allowfullscreen>
							</iframe>

						</div>
					</div>
					<div class="col-12 col-lg-4 pt-3 pt-lg-0">
						<div class="card duke-bg-tertiary-gray3 rounded shadow">
							<div class="card-body">
								<div class="card-title rounded p-2 h5 duke-bg-purple text-white">
									<?php
									$field_subtitle = field_view_field('node', $node, 'field_subtitle', $display);
									print drupal_render($field_subtitle);
									?>
								</div>
								<div class="card-text">
									<?php
									$field_description = field_view_field('node', $node, 'field_description', $display);
									print drupal_render($field_description);
									?>
								</div>
							</div>
						</div>
					</div>


				</div>
			</div>
		</div>


	</div>
</div>
