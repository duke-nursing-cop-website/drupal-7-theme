<header>
  <div class="wrapper">
    <nav class="header navbar navbar-dark duke-bg-blue pr-0">
      <div class="row w-100 align-items-center">
        <div class="col-5 order-1 col-md-8 col-lg-2">
          <div class="navbar-brand ">
            <a href="https://nursing.duke.edu">
              <img src="https://nursing.duke.edu/sites/default/files/duke-nursing-logo.png ">
            </a>
          </div>
        </div>
        <div class="col-10 col-lg-7 mt-1 order-3  order-lg-2 d-flex justify-content-lg-center ">
          <div class="cop-logo">
            <a href="/">
              <img src="<?php print theme_get_setting('logo', 'copdukenursing'); ?>" alt="" />
              <div>communities of practice</div>
            </a>
          </div>
        </div>
        <div class="d-none d-lg-flex col-6 col-md-3 order-2 order-lg-3 col-lg-2  px-0 justify-content-end">
          <a class="white-button" href="<?php print theme_get_setting('become_a_preceptor_url');  ?>">
            Become a Preceptor
          </a>
        </div>
        <div class="col-2 col-lg-1 order-4 d-flex justify-content-end">
          <div class="open-menu toggler-icon-wrapper">
            <div class="navbar-toggler-icon"></div>
          </div>
        </div>
      </div>
    </nav>
  </div>

  <nav class="d-none d-lg-block navbar navbar-expand-lg navbar-dark bg-primary">
    <div class="collapse navbar-collapse">
      <?php if ($content) : ?>
        <div class="<?php print $classes; ?>">
          <?php print $content; ?>
        </div>
      <?php endif; ?>
    </div>
  </nav>


  <div class="overlay">
    <div id="menu-container">
      <div class="navbar-dark duke-bg-blue">
        <div class="row h-100">
          <div class="h-100 col-10 d-flex align-items-center justify-content-center pr-0 pt-3">
            <div class="cop-logo">
              <a href="/">
                <img src="<?php print theme_get_setting('logo', 'copdukenursing'); ?>" alt="" />
                <div>communities of practice</div>
              </a>
            </div>
          </div>
          <div class="col-2 px-0">
            <a href="javascript:void()" class="dismiss d-flex h-100 justify-content-center align-items-center">
              <i class="fa fa-plus"></i>
            </a>
          </div>
        </div>
      </div>
      <div class="search-wrapper">
        <form class="search form-inline flex-nowrap w-100 mx-auto">
          <input class="form-control w-100" type="text" placeholder="Search Clinical Modules" />
          <button class="px-0 mx-0">
            <i class=" fas fa-search"></i>
          </button>
        </form>
      </div>

      <?php if ($content) : ?>
        <div class="<?php print $classes; ?>">
          <?php print $content; ?>
        </div>
      <?php endif; ?>
    </div>
  </div>
</header>