

<?php  $edit_mode = isset($variables['page']['content']['system_main']['#node_edit_form']); ?>

<?php if ($logged_in) : ?>
<div id="navigation">

  <?php if ($primary_local_tasks): ?>
    <?php print render($primary_local_tasks); ?>
	<?php endif; ?>

  <?php if ($secondary_local_tasks): ?>
    <div class="tabs-secondary clearfix"><ul class="tabs secondary"><?php print render($secondary_local_tasks); ?></ul></div>
  <?php endif; ?>

</div>
<?php endif; ?>


<?php if (! $edit_mode) : ?>
<?php print render($page['header']); ?>
<?php endif; ?>


<div id="content">
<?php  print render($page['content']) ?>
</div>

<?php if (! $edit_mode): ?>
<?php print render($page['footer']); ?>
<?php endif; ?>
